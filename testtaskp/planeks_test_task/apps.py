from django.apps import AppConfig


class PlaneksTestTaskConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'planeks_test_task'
