from django.urls import path
from . import views

urlpatterns = [
    path('', views.login, name = "login-page"),
    path('dataschemas', views.DataSchemas, name = "dataschemas-page"),
    path('newschema', views.NewSchema, name = "newschema-page"),
    path('datasets', views.DataSets, name = "datasets-page")
]
