from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect

# Create your views here.

def login(request):
    return render(request, "planeks_test_task/login.html")


def DataSchemas(request):
    return render(request, "planeks_test_task/dataschemas.html")

def NewSchema(request):
    return render(request, "planeks_test_task/newschema.html")

def DataSets(request):
    return render(request, "planeks_test_task/datasets.html")

